package com.kusandriadi.crud.model;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.kusandriadi.crud.beans.Biodata;
import com.kusandriadi.crud.dao.BiodataDao;
import com.kusandriadi.crud.dao.injection.BiodataDaoInjection;

public class BiodataManipulation {
	
	private BiodataDao dao = new BiodataDaoInjection();
	
	private String nim;
	
	private String nama;
	
	private String jurusan;
	
	private String alamat;
	
	public BiodataManipulation(){
		
	}

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public void setDao(BiodataDao dao) {
		this.dao = dao;
	}
	
	public void saveBiodata(){
		try{
			Biodata biodata = new Biodata();
			biodata.setNim(nim);
			biodata.setNama(nama);
			biodata.setJurusan(jurusan);
			biodata.setAlamat(alamat);
			dao.saveBiodata(biodata);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Berhasil!", "Data Telah Disimpan"));  
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void updateBiodata(){
		try{
			Biodata biodata = new Biodata();	
			biodata.setNim(nim);
			biodata.setNama(nama);
			biodata.setJurusan(jurusan);
			biodata.setAlamat(alamat);
			dao.updateBiodata(biodata);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Berhasil!", "Data Telah Di update")); 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void deleteBiodata(){
		try{
			List<Biodata> list = dao.getBiodata(nim);
			Biodata biodata = new Biodata();
			for(Biodata bio : list){
				biodata = new Biodata();
				biodata.setNim(bio.getNim());
				biodata.setNama(bio.getNama());
				biodata.setJurusan(bio.getJurusan());
				biodata.setAlamat(bio.getAlamat());
			}
			dao.deleteBiodata(biodata);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Berhasil!", "Data Telah Di hapus"));  
		}catch(Exception e){
			
		}
	}
	
	public void searchBiodata(){	
		ArrayList<Biodata> list = dao.searchBiodata(nim);
		for(Biodata bio : list){
			setNim(bio.getNim());
			setNama(bio.getNama());
			setJurusan(bio.getJurusan());
			setAlamat(bio.getAlamat());
		}
	}
	
	public void clear(){
		setNim("");
		setNama("");
		setAlamat("");
		setJurusan("");
	}
}
