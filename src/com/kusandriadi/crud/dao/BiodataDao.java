package com.kusandriadi.crud.dao;

import java.util.ArrayList;
import java.util.List;

import com.kusandriadi.crud.beans.Biodata;

public interface BiodataDao {
	
	void saveBiodata(Biodata biodata);
	
	void deleteBiodata(Biodata biodata);
	
	void updateBiodata(Biodata biodata);
	
	List<Biodata> getBiodata(String nim);
	
	ArrayList<Biodata> searchBiodata(String nim);

}
