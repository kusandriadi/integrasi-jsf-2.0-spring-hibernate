package com.kusandriadi.crud.dao.injection;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kusandriadi.crud.beans.Biodata;
import com.kusandriadi.crud.dao.BiodataDao;

@Repository("biodataDao")
public class BiodataDaoInjection extends HibernateDaoSupport implements BiodataDao{
	
	@Autowired
	public void setSession(SessionFactory sessionFactory){
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW)
	public void deleteBiodata(Biodata biodata) {
		try {
			getHibernateTemplate().delete(biodata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true,propagation=Propagation.REQUIRES_NEW)
	public List<Biodata> getBiodata(final String nim) {
		try {
			return getHibernateTemplate().find("from Biodata where nim = ?", nim);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
	public void saveBiodata(Biodata biodata) {
		try {
			getHibernateTemplate().save(biodata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW)
	public void updateBiodata(Biodata biodata) {
		try {
			getHibernateTemplate().update(biodata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Biodata> searchBiodata(String nim) {
		try {
			return (ArrayList<Biodata>) getHibernateTemplate().find(
					"FROM Biodata WHERE nim = ?", nim);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
