package com.kusandriadi.crud.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Biodata implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	private String nim;
	@Column(nullable=false, length=25)
	private String nama;
	@Column(length=10)
	private String jurusan;
	@Column(length=50)
	private String Alamat;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getAlamat() {
		return Alamat;
	}

	public void setAlamat(String alamat) {
		Alamat = alamat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Alamat == null) ? 0 : Alamat.hashCode());
		result = prime * result + ((jurusan == null) ? 0 : jurusan.hashCode());
		result = prime * result + ((nama == null) ? 0 : nama.hashCode());
		result = prime * result + ((nim == null) ? 0 : nim.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Biodata other = (Biodata) obj;
		if (Alamat == null) {
			if (other.Alamat != null)
				return false;
		} else if (!Alamat.equals(other.Alamat))
			return false;
		if (jurusan == null) {
			if (other.jurusan != null)
				return false;
		} else if (!jurusan.equals(other.jurusan))
			return false;
		if (nama == null) {
			if (other.nama != null)
				return false;
		} else if (!nama.equals(other.nama))
			return false;
		if (nim == null) {
			if (other.nim != null)
				return false;
		} else if (!nim.equals(other.nim))
			return false;
		return true;
	}
	
	

}
